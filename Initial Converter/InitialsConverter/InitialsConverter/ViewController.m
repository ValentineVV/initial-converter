//
//  ViewController.m
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/2/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "ViewController.h"
#import "GalleryViewController.h"
#import "StartModel.h"
#import "StartViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *galleryImage;
@property (weak, nonatomic) IBOutlet UIImageView *examplesImage;
@property (weak, nonatomic) IBOutlet UIImageView *startImage;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *galleryRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(galleryTapped)];
    [self.galleryImage addGestureRecognizer:galleryRecognizer];
    self.galleryImage.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *examplesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(examplesTapped)];
    [self.examplesImage addGestureRecognizer:examplesRecognizer];
    self.examplesImage.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *startRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(startTapped)];
    [self.startImage addGestureRecognizer:startRecognizer];
    self.startImage.userInteractionEnabled = YES;
    self.mainParam = [[MainParametres alloc] init];
    
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)galleryTapped {
    
    UIViewController *view  =[self.storyboard instantiateViewControllerWithIdentifier:@"gallery"];
    [self.navigationController pushViewController:view animated:YES];
}

-(void)examplesTapped {
    
    UIViewController *view  =[self.storyboard instantiateViewControllerWithIdentifier:@"examples"];
    [self.navigationController pushViewController:view animated:YES];
}

-(void)startTapped {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Create Handkerchief" message:@"Please, enter your full name.\nIt must consist of 2 or 3 words." preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil ]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Start" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        NSString *initials = [StartModel getInitials:alert.textFields[0].text];
        if (initials) {
            StartViewController *view  =[self.storyboard instantiateViewControllerWithIdentifier:@"start"];
//            view.initials = initials;
            self.mainParam.initials = initials;
            self.mainParam.fullName = alert.textFields[0].text;
            view.mainParam = self.mainParam;
            [self.navigationController pushViewController:view animated:YES];
        }
    }]];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
    }];
    [self presentViewController:alert animated:YES completion:nil];

}


@end
