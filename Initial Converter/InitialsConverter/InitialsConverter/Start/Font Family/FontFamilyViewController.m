//
//  FontFamilyViewController.m
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/17/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "FontFamilyViewController.h"
#import "StartTableViewCell.h"
#import "StartModel.h"
#import "Background.h"

@interface FontFamilyViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *table;

@end

@implementation FontFamilyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Font Family";
    [self.table registerNib:[UINib nibWithNibName:@"StartTableViewCell" bundle:nil] forCellReuseIdentifier:@"fontFamily"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    StartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"fontFamily"];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    [cell setInitials:self.mainParam.initials forRow:indexPath.row controllerNumber:([self.navigationController.viewControllers count] - 1) withParams:self.mainParam];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 180;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    StartModel *model = [[StartModel alloc] init];
    self.mainParam.fontFamily = [model getFontNames][indexPath.row];
    
    Background *background = [self.storyboard instantiateViewControllerWithIdentifier:@"background"];
    background.mainParam = self.mainParam;
    [self.navigationController pushViewController:background animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
