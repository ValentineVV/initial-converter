//
//  StartViewController.m
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/11/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "StartViewController.h"
#import "StartTableViewCell.h"
#import "StartModel.h"
#import "Preview.h"

@interface StartViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (nonatomic, strong) StartModel *model;
@end


typedef enum ControllerNumber : NSUInteger {
    patterns = 1,
    fontColors = 2,
    fontFamily = 3,
    background = 4
    
} ContorllerNumber;

@implementation StartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.model = [[StartModel alloc] init];
    self.navigationItem.title = @"Patterns";
    [self.table registerNib:[UINib nibWithNibName:@"StartTableViewCell" bundle:nil] forCellReuseIdentifier:@"startCell"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.model.labelStyles[([self.navigationController.viewControllers count] - 2)] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    StartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"startCell"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    [cell setInitials:self.mainParam.initials forRow:indexPath.row controllerNumber:([self.navigationController.viewControllers count] - 1) withParams:self.mainParam];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    StartViewController *start = [self.storyboard instantiateViewControllerWithIdentifier:@"start"];
    switch ([self.navigationController.viewControllers count] - 1) {
        case patterns:{
            NSMutableArray<NSArray*> *arr = [[NSMutableArray alloc] init];;
            for (int i = 0; i < [self.mainParam.initials length]; i++){
                [arr addObject:[self.model getAnchorsForController:[self.navigationController.viewControllers count] - 1 row:indexPath.row letter:i param:self.mainParam]];
            }
            self.mainParam.anchors = [arr copy];
            break;
        }
        case fontColors:
            self.mainParam.initialsColor = [self.model getInitialsColors][indexPath.row];
            break;
        case fontFamily:
            self.mainParam.fontFamily = [self.model getFontNames][indexPath.row];
            break;
        case background:
            self.mainParam.background = [self.model getColorsForBackground][indexPath.row];
            Preview *preview = [self.storyboard instantiateViewControllerWithIdentifier:@"preview"];
            preview.mainParam = self.mainParam;
            [self.navigationController pushViewController:preview animated:YES];
            break;
    }
    if ([self.navigationController.viewControllers count] - 2 != background) {
        start.mainParam = self.mainParam;
        [self.navigationController pushViewController:start animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 180;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
