//
//  startModel.m
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/7/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "StartModel.h"
#import <UIKit/UIKit.h>
#import "MainParametres.h"

typedef enum ControllerNumber : NSUInteger {
    patterns = 1,
    fontColors = 2,
    fontFamily = 3,
    background = 4
    
} ContorllerNumber;

@implementation StartModel

#define STORED_DATA_FOLDER_NAME @"Destination"

-(instancetype)init {
    
    if (self = [super init]) {
        self.labelStyles = @[@[@"STRETCH", @"LEFT CASCADE", @"RIGHT CASCADE", @"STRAIGHT"],
                             @[@"CHARCOAL", @"BLIND GOLD", @"MOONLIGHT", @"LOFT", @"ROSE", @"DIRTY GRASS"],
                             @[@"DANCING SCRIPT", @"RIGHTEOUS", @"SHADOWS INTO LIGHT", @"SPECIAL ELITE", @"PRESS START 2P"],
                             @[@"RED WINE", @"EARLY PEACH", @"GREEN DREAM", @"SPRING PURPLE"]];
    }
    
    return self;
}

+(NSString *)getInitials:(NSString *)fullName {
    
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z]" options:0 error:&error];
    NSArray *matches = [regex matchesInString:fullName options:0 range:NSMakeRange(0, [fullName length])];
    NSString* matchText = @"";
    for (NSTextCheckingResult *match in matches) {
        matchText = [matchText stringByAppendingString:[fullName substringWithRange:[match range]]];
    }

    if ([matchText length] == 2 || [matchText length] == 3) {
        return matchText;
    } else {
        return nil;
    }
    
}

-(NSArray<NSArray<NSNumber*>*>*)getAnchorXForInitials:(NSInteger)ammount {
    if (ammount == 3){
        return @[@[@-80, @0, @80], @[@-60, @0, @60], @[@-60, @0, @60], @[@-60, @0, @60]];
    } else {
        return @[@[@-40, @40], @[@-30, @30], @[@-30, @30], @[@-30, @30]];
    }
    
}

-(NSArray<NSArray<NSNumber*>*>*)getAnchorYForInitials:(NSInteger)ammount {
    if (ammount == 3){
        return @[@[@-17.5, @-17.5, @-17.5], @[@-17.5, @-35, @-52.5], @[@-52.5, @-35, @-17.5], @[@-17.5, @-17.5, @-17.5]];
    } else {
        return @[@[@-17.5, @-17.5], @[@-22.5, @-52.5], @[@-52.5, @-22.5], @[@-17.5, @-17.5]];
    }
}

-(NSArray<UIColor*>*)getInitialsColors {
    
    return @[[UIColor colorWithRed:72./255 green:72./255 blue:72./255 alpha:1],
             [UIColor colorWithRed:214./255 green:186./255 blue:96./255 alpha:1],
             [UIColor colorWithRed:162./255 green:192./255 blue:197./255 alpha:1],
             [UIColor colorWithRed:149./255 green:151./255 blue:155./255 alpha:1],
             [UIColor colorWithRed:202./255 green:155./255 blue:137./255 alpha:1],
             [UIColor colorWithRed:60./255 green:76./255 blue:54./255 alpha:1]];
}

- (UILabel *)getLabelForController:(NSInteger)number row:(NSInteger)row {
    UILabel *labelStyle = [[UILabel alloc] init];
    labelStyle.text = self.labelStyles[number - 1][row];
    labelStyle.font = [UIFont systemFontOfSize:12];
    labelStyle.textColor = [UIColor colorWithRed:72./255 green:72./255 blue:72./255 alpha:1];
    labelStyle.translatesAutoresizingMaskIntoConstraints = NO;
    return labelStyle;
}

-(NSArray<UIFont*>*)getFontNames {
    
    return @[[UIFont fontWithName:@"DancingScript-Regular" size:78],
             [UIFont fontWithName:@"Righteous-Regular" size:78],
             [UIFont fontWithName:@"ShadowsIntoLight" size:78],
             [UIFont fontWithName:@"SpecialElite-Regular" size:78],
             [UIFont fontWithName:@"PressStart2P-Regular" size:78]];
}

-(UILabel *)getLabelInitialsForController:(NSInteger)number row:(NSInteger)row character:(NSString *)character mainParametres:(MainParametres*)mainParam {
    
    UILabel *label = [[UILabel alloc] init];
    switch (number) {
        case patterns:
            label.font = [UIFont systemFontOfSize:78];
            label.textColor = [UIColor colorWithRed:72./255 green:72./255 blue:72./255 alpha:1];
            break;
        case fontColors:
            label.font = [UIFont systemFontOfSize:78];
            label.textColor = [self getInitialsColors][row];
            break;
        case fontFamily:
            label.textColor = mainParam.initialsColor;
            label.font = [self getFontNames][row];
            break;
        case background:
            label.textColor = mainParam.initialsColor;
            label.font = mainParam.fontFamily;
            break;
    }
    label.text = character;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    
    return label;
}

-(NSArray<NSNumber*>*)getAnchorsForController:(NSInteger)number row:(NSInteger)row letter:(NSInteger)i param:(MainParametres *)mainParam {
    

    NSArray<NSNumber*> *anchors;
    
    switch (number) {
        case patterns:
                anchors = [[NSArray alloc] initWithObjects:[self getAnchorXForInitials:[mainParam.initials length]][row][i], [self getAnchorYForInitials:[mainParam.initials length]][row][i], nil];
            break;
        case fontColors:
        case fontFamily:
        case background:
            anchors = [[NSArray alloc] initWithObjects:mainParam.anchors[i][0], mainParam.anchors[i][1], nil];
            break;
    }
    
    return anchors;
}

-(NSArray<UIColor*>*)getColorsForBackground {
    
    return @[[UIColor colorWithRed:255./255 green:96./255 blue:96./255 alpha:1],
             [UIColor colorWithRed:255./255 green:219./255 blue:106./255 alpha:1],
             [UIColor colorWithRed:98./255 green:215./255 blue:203./255 alpha:1],
             [UIColor colorWithRed:85./255 green:132./255 blue:228./255 alpha:1]];
}

-(UIView *)getLine {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(60, 116, 250, 1)];
    view.backgroundColor = [UIColor colorWithRed:151./255 green:151./255 blue:151./255 alpha:1];
    return view;
}

-(UIView*)getBackgroundForController:(NSInteger)number row:(NSInteger)row {
    UIView* back = [[UIView alloc] initWithFrame:CGRectMake(60, 0, 250, 116)];
    if (number == background) {
        back.backgroundColor = [self getColorsForBackground][row];
    }
    return back;
}

-(void)saveData:(UIImage*)img personFullName:(NSString*)fullName {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *documentsURL = [fileManager URLsForDirectory:NSLibraryDirectory inDomains:NSUserDomainMask].firstObject;
    NSString *storedDataPath = [documentsURL.path stringByAppendingPathComponent:STORED_DATA_FOLDER_NAME];
    NSError *error;
    BOOL isDir;
    if (![fileManager fileExistsAtPath:storedDataPath isDirectory:&isDir]){
    [fileManager createDirectoryAtPath:storedDataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    if (error) {
        NSLog(@"%@", error);
    }
    NSData *dat = UIImageJPEGRepresentation(img, 1);
    NSInteger randomNumber = arc4random() % 1000;
    NSString *fileName = [[fullName stringByAppendingString: [NSString stringWithFormat:@"+%ld", randomNumber]] stringByAppendingPathExtension:@"jpeg"];

    [fileManager createFileAtPath:[storedDataPath stringByAppendingPathComponent:fileName] contents:dat attributes:nil];
    
}

-(NSArray<NSString*>*)getFilePaths {
    
    NSFileManager *manager = [NSFileManager defaultManager];
    NSURL *docURL = [manager URLsForDirectory:NSLibraryDirectory inDomains:NSUserDomainMask].firstObject;
    NSString *sourcePath = [docURL.path stringByAppendingPathComponent:STORED_DATA_FOLDER_NAME];
    
    NSArray *arr = [manager contentsOfDirectoryAtPath:sourcePath error:NULL];
    NSMutableArray *JPEGFiles = [[NSMutableArray alloc] init];
    [arr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *filename = (NSString*)obj;
        NSString *extension = [[filename pathExtension] lowercaseString];
        if ([extension isEqualToString:@"jpeg"]) {
            [JPEGFiles addObject:[sourcePath stringByAppendingPathComponent:filename]];
        }
    }];
        
    return JPEGFiles;
}

@end
