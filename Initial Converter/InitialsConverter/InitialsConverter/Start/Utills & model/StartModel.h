//
//  startModel.h
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/7/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UILabel;
@class UIColor;
@class UIFont;
@class UIView;
@class MainParametres;
@class UIImage;

@interface StartModel : NSObject

//@property (strong, nonatomic) NSArray<NSArray<NSNumber*>*>* anchorX;
//@property (strong, nonatomic) NSArray<NSArray<NSNumber*>*>* anchorY;
@property (nonatomic, strong) NSArray<NSArray<NSString*>*> *labelStyles;

+(NSString*) getInitials:(NSString*)fullName;
//+(NSArray<NSArray<NSNumber*>*>*)getAnchorX;
//+(NSArray<NSArray<NSNumber*>*>*)getAnchorY;

-(UIView*)getLine;
-(UILabel*)getLabelForController:(NSInteger)number row:(NSInteger)row;
-(UILabel*)getLabelInitialsForController:(NSInteger)number row:(NSInteger)row character:(NSString*)character mainParametres:(MainParametres*)mainParam;

-(NSArray<NSNumber*>*)getAnchorsForController:(NSInteger)number row:(NSInteger)row letter:(NSInteger)i param:(MainParametres*)mainParam;
-(NSArray<UIColor*>*)getInitialsColors;
-(NSArray<UIFont*>*)getFontNames;
-(UIView*)getBackgroundForController:(NSInteger)number row:(NSInteger)row;
-(NSArray<UIColor*>*)getColorsForBackground;
-(void)saveData:(UIImage*)img personFullName:(NSString*)fullName;
-(NSArray<NSString*>*)getFilePaths;

@end
