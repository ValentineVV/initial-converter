//
//  MainParametres.h
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/16/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIColor;
@class UIFont;

@interface MainParametres : NSObject

@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *initials;
@property (nonatomic, strong) UIColor *initialsColor;
@property (nonatomic, strong) NSArray<NSArray<NSNumber*>*> *anchors;
@property (nonatomic, strong) UIColor *background;
@property (nonatomic, strong) UIFont *fontFamily;

@end
