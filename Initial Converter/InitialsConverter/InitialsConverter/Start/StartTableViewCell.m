//
//  StartTableViewCell.m
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/8/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "StartTableViewCell.h"
#import "StartModel.h"

@interface StartTableViewCell ()

//@property(nonatomic, strong) NSArray<NSLayoutConstraint*> *constraints;
@property (weak, nonatomic) IBOutlet UIView *view;

@property (strong, nonatomic) NSArray<NSString*> *styles;
@property (strong, nonatomic) NSArray<NSArray<NSNumber*>*>* anchorX;
@property (strong, nonatomic) NSArray<NSArray<NSNumber*>*>* anchorY;

@end

@implementation StartTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
//    self.styles = @[@"STRETCH", @"LEFT CASCADE", @"RIGHT CASCADE", @"STRAIGHT"];
//    self.anchorX = [StartModel getAnchorX];
//    self.anchorY = [StartModel getAnchorY];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setInitials:(NSString*)initials forRow:(NSInteger)row controllerNumber:(NSInteger)number withParams:(MainParametres *)mainParam {
    
    StartModel *model = [[StartModel alloc] init];
    
    UILabel *labelStyle = [model getLabelForController:number row:row];
    [self.view addSubview:labelStyle];
    [[labelStyle.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor] setActive:YES];
    [[labelStyle.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor constant:43] setActive:YES];
    
    UIView *background = [model getBackgroundForController:number row:row];
    [self.view addSubview:background];
    [[background.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor] setActive:YES];
    [[background.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:5] setActive:YES];
    
    UIView *view = [model getLine];
    [self.view addSubview:view];
    [[view.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor] setActive:YES];
    
    for (NSInteger i = 0; i < [mainParam.initials length]; i++) {
        UILabel *label = [model getLabelInitialsForController:number row:row character:[NSString stringWithFormat:@"%C", [mainParam.initials characterAtIndex:i]] mainParametres:mainParam];
        [self.view addSubview:label];
        NSArray<NSNumber*> *anchors = [model getAnchorsForController:number row:row letter:i param:mainParam];
        
        [[label.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor constant:[anchors[0] intValue]] setActive:YES];
        [[label.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor constant:[anchors[1] doubleValue]] setActive:YES];        
    }
    
    
}

- (void)prepareForReuse {
    [super prepareForReuse];
    for (UIView* l in self.view.subviews) {
            [l removeFromSuperview];
    }
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(60, 116, 250, 1)];
    view.backgroundColor = [UIColor colorWithRed:151./255 green:151./255 blue:151./255 alpha:1];
    [self.view addSubview:view];
    
    [[view.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor] setActive:YES];
}



@end
