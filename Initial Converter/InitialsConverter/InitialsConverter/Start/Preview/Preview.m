//
//  Preview.m
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/21/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "Preview.h"
#import "StartModel.h"
#import <MessageUI/MessageUI.h>
#import "GalleryViewController.h"

@interface Preview () <MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) UIView* viewForCutting;

@end

@implementation Preview

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Preview";
    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont boldSystemFontOfSize:27];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:label];
    [[label.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:34] setActive:YES];
    [[label.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:105] setActive:YES];
    if (!self.img){
        
        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style: UIBarButtonItemStylePlain                                                                     target:self action:@selector(save)];
        self.navigationItem.rightBarButtonItem = anotherButton;
        
        label.text = self.mainParam.fullName;
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(34, 166, 345, 420)];
        view.backgroundColor = self.mainParam.background;
        [self.view addSubview:view];
        for (int i = 0; i < [self.mainParam.initials length]; i++) {
            
            UILabel *initial = [[UILabel alloc] init];
            initial.text = [NSString stringWithFormat:@"%c",[self.mainParam.initials characterAtIndex:i]];
            initial.textColor = self.mainParam.initialsColor;
            initial.font = self.mainParam.fontFamily;
            initial.translatesAutoresizingMaskIntoConstraints = NO;
            [view insertSubview:initial aboveSubview:view];
            [[initial.centerXAnchor constraintEqualToAnchor:view.centerXAnchor constant:[self.mainParam.anchors[i][0] integerValue]*1.5]setActive:YES];
            double avr = 0;
            for (int i = 0; i < [self.mainParam.initials length]; i++) {
                avr += [self.mainParam.anchors[i][1] integerValue];
            }
            avr /= [self.mainParam.initials length];
                [[initial.centerYAnchor constraintEqualToAnchor:view.centerYAnchor constant:([self.mainParam.anchors[i][1] integerValue] - avr)*4]setActive:YES];
        }
        self.viewForCutting = view;
    } else {
        label.text = self.fullName;
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(34, 166, 345, 420)];
        imgView.image = self.img;
        [self.view addSubview:imgView];
        UIBarButtonItem *actionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actions)];
        self.navigationItem.rightBarButtonItem = actionButton;
        
    }
    
    // Do any additional setup after loading the view.
}

-(void)actions{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Save to Fotos" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            UIImageWriteToSavedPhotosAlbum(self.img, nil, nil, nil);
        });
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Send via Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (![MFMailComposeViewController canSendMail]) {
            NSLog(@"Mail services are not available.");
            return;
        }
        
        MFMailComposeViewController* composeVC = [[MFMailComposeViewController alloc] init];
        composeVC.mailComposeDelegate = self;
        
        // Configure the fields of the interface.
        [composeVC setToRecipients:@[@"address@example.com"]];
        [composeVC setSubject:@"Amazing app!!!"];
        [composeVC setMessageBody:@"Hello, just have a look at this wonderfull image from my new app!" isHTML:NO];
        NSData *image = UIImageJPEGRepresentation(self.img, 1);
        [composeVC addAttachmentData:image mimeType:@"image/jpeg" fileName:self.fullName];
        
        // Present the view controller modally.
        [self presentViewController:composeVC animated:YES completion:nil];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self.galView deleteCellAtIndexPath:self.indexPath];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    if (error) {
        NSLog(@"Unable to send Email due to error: %@", error);
    }
    
    // Dismiss the mail compose view controller.
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)save {
    CGRect rect = [self.viewForCutting bounds];
    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.viewForCutting.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    StartModel *model = [[StartModel alloc] init];
    [model saveData:img personFullName:self.mainParam.fullName];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
