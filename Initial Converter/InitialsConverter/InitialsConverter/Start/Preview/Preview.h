//
//  Preview.h
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/21/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainParametres.h"

@class GalleryViewController;

@interface Preview : UIViewController

@property (nonatomic, strong) MainParametres *mainParam;
@property (nonatomic, strong) UIImage *img;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, weak) GalleryViewController *galView;

@end
