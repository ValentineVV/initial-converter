//
//  Background.m
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/18/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "Background.h"
#import "StartTableViewCell.h"
#import "StartModel.h"
#import "Preview.h"

@interface Background () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *table;

@end

@implementation Background

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Background";
    [self.table registerNib:[UINib nibWithNibName:@"StartTableViewCell" bundle:nil] forCellReuseIdentifier:@"background"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    StartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"background"];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    [cell setInitials:self.mainParam.initials forRow:indexPath.row controllerNumber:([self.navigationController.viewControllers count] - 1) withParams:self.mainParam];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Preview *preview = [self.storyboard instantiateViewControllerWithIdentifier:@"preview"];
    StartModel *model = [[StartModel alloc] init];
    self.mainParam.background = [model getColorsForBackground][indexPath.row];
    preview.mainParam = self.mainParam;
    [self.navigationController pushViewController:preview animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 180;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
