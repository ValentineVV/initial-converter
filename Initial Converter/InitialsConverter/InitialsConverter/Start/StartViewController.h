//
//  StartViewController.h
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/11/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainParametres.h"

@interface StartViewController : UIViewController

@property (nonatomic, strong) MainParametres *mainParam;

@end
