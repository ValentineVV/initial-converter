//
//  FontColors.m
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/14/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "FontColors.h"
#import "StartTableViewCell.h"
#import "StartModel.h"
#import "FontFamilyViewController.h"
#import "Preview.h"

@interface FontColors () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (nonatomic, strong) StartModel *model;

@end


typedef enum ControllerNumber : NSUInteger {
    patterns = 1,
    fontColors = 2,
    fontFamily = 3,
    background = 4
    
} ContorllerNumber;

@implementation FontColors

- (void)viewDidLoad {
    [super viewDidLoad];
    self.model = [[StartModel alloc] init];
    self.navigationItem.title = @"Font Colors";
    [self.table registerNib:[UINib nibWithNibName:@"StartTableViewCell" bundle:nil] forCellReuseIdentifier:@"fontColorCell"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch ([self.navigationController.viewControllers count] - 1) {
        case fontColors:
            return [[self.model getInitialsColors] count];
            break;
        case fontFamily:
            return [[self.model getFontNames] count];
            break;
        case background:
            return [[self.model getColorsForBackground] count];
            break;
            
        default:
            break;
    }
    return 0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    StartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"fontColorCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    [cell setInitials:self.mainParam.initials forRow:indexPath.row controllerNumber:([self.navigationController.viewControllers count] - 1) withParams:self.mainParam];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 180;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FontColors *fontColorsController = [self.storyboard instantiateViewControllerWithIdentifier:@"fontColor"];
    switch ([self.navigationController.viewControllers count] - 1) {
        case fontColors:
            self.mainParam.initialsColor = [self.model getInitialsColors][indexPath.row];
            break;
        case fontFamily:
            self.mainParam.fontFamily = [self.model getFontNames][indexPath.row];
            break;
        case background:
            self.mainParam.background = [self.model getColorsForBackground][indexPath.row];
            Preview *preview = [self.storyboard instantiateViewControllerWithIdentifier:@"preview"];
            preview.mainParam = self.mainParam;
            [self.navigationController pushViewController:preview animated:YES];
            break;
    }
    if ([self.navigationController.viewControllers count] - 2 != background) {
        fontColorsController.mainParam = self.mainParam;
        [self.navigationController pushViewController:fontColorsController animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
