//
//  FontColors.h
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/14/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainParametres.h"

@interface FontColors : UIViewController

@property (nonatomic, strong) MainParametres *mainParam;

@end
