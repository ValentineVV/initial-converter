//
//  StartTableViewCell.h
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/8/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainParametres.h"

@interface StartTableViewCell : UITableViewCell

- (void)setInitials:(NSString*)initials forRow:(NSInteger)row controllerNumber:(NSInteger)number withParams:(MainParametres*) mainParam;

@end
