//
//  startModel.m
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/7/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "StartModel.h"

@implementation StartModel

+(NSString *)getinitials:(NSString *)fullName {
    
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z]" options:NSRegularExpressionCaseInsensitive error:&error];
//    NSArray *matches = [regex matchesInString:fullName options:0 range:NSMakeRange(0, [fullName length])];
    NSString *result = [regex stringByReplacingMatchesInString:fullName options:0 range:NSMakeRange(0, [fullName length]) withTemplate:@"$0"];
    if ([result length] == 2 || [result length] == 3) {
        return result;
    } else {
        return nil;
    }
}

@end
