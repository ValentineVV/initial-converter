//
//  GalleryCellTableViewCell.m
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/23/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "GalleryCellTableViewCell.h"

@implementation GalleryCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
