//
//  GalleryCellTableViewCell.h
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/23/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryCellTableViewCell : UITableViewCell

@end
