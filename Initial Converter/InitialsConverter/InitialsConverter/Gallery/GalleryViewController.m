//
//  GalleryViewController.m
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/2/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "GalleryViewController.h"
#import "StartModel.h"
#import "GalleryCell.h"
#import "Preview.h"

@interface GalleryViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (nonatomic, strong) NSArray<NSString*> *fields;

@end

@implementation GalleryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Gallery";
    StartModel * model = [[StartModel alloc] init];
    self.fields = [[model getFilePaths] sortedArrayUsingComparator:^NSComparisonResult(NSString *obj1, NSString *obj2) {
//        NSDate *date1 =
        NSDictionary* fileAttribs1 = [[NSFileManager defaultManager] attributesOfItemAtPath:obj1 error:nil];
        NSDate *date1 = [fileAttribs1 objectForKey:NSFileCreationDate];
        NSDictionary* fileAttribs2 = [[NSFileManager defaultManager] attributesOfItemAtPath:obj2 error:nil];
        NSDate *date2 = [fileAttribs2 objectForKey:NSFileCreationDate];
        return [date1 compare:date2];
    }];
    [self.table registerNib:[UINib nibWithNibName:@"GalleryCell" bundle:nil] forCellReuseIdentifier:@"galleryCell"];
    
    
    // Do any additional setup after loading the view.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Preview *preview = [self.storyboard instantiateViewControllerWithIdentifier:@"preview"];
//    StartModel *model = [[StartModel alloc] init];
    GalleryCell * galCell = (GalleryCell*)[tableView cellForRowAtIndexPath:indexPath];
    preview.fullName = galCell.name.text;
    preview.img = galCell.image.image;
    preview.indexPath = indexPath;
    preview.galView = self;
    [self.navigationController pushViewController:preview animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.fields count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 116;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GalleryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"galleryCell"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    NSData *dat = [NSData dataWithContentsOfFile:self.fields[indexPath.row]];
    cell.image.image = [UIImage imageWithData:dat];
    
    cell.name.text = [[[[self.fields objectAtIndex:indexPath.row] lastPathComponent] stringByDeletingPathExtension]componentsSeparatedByString:@"+"][0];
    NSDictionary* fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath:[self.fields objectAtIndex:indexPath.row] error:nil];
    NSDate *result = [fileAttribs objectForKey:NSFileCreationDate];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMMM, yyyy"];
    cell.date.text = [formatter stringFromDate:result];
    
    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:self.fields[indexPath.row] error:&error];
    if (error) {
        NSLog(@"Can't delete file at path %@", self.fields[indexPath.row]);
        return;
    }
    StartModel * model = [[StartModel alloc] init];
    self.fields = [model getFilePaths];
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
}

-(void) deleteCellAtIndexPath:(NSIndexPath *)indexPath {
    [self tableView:self.table commitEditingStyle:UITableViewCellEditingStyleDelete forRowAtIndexPath:indexPath];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
