//
//  PageContentViewController.h
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/4/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageContentViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameView;
@property (nonatomic, assign) NSInteger pageIndex;
@property (strong, nonatomic) NSString *nameLabel;
@property (nonatomic, strong) NSString *imageFile;

@end
