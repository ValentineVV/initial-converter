//
//  exampleViewController.m
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/7/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "exampleViewController.h"
#import "PageViewController.h"

@interface exampleViewController ()

@property (nonatomic, strong) PageViewController *pageViewController;

@end

@implementation exampleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
