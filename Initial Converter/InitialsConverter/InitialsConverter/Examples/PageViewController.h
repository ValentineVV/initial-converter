//
//  PageViewController.h
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/4/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageViewController : UIPageViewController

@property (nonatomic, strong) NSArray<NSString*> *pageNames;
@property (nonatomic, strong) NSArray<NSString*> *pageImages;

@end
