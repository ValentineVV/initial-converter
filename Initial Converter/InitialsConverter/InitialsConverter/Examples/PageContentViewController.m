//
//  PageContentViewController.m
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/4/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "PageContentViewController.h"

@interface PageContentViewController ()

@end

@implementation PageContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.nameView.text = self.nameLabel;
//    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:self.imageFile]];
//    [self.imageView initWithImage:[UIImage imageNamed:self.imageFile]];
    self.imageView.image = [UIImage imageNamed:self.imageFile];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
