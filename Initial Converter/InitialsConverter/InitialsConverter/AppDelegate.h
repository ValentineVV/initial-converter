//
//  AppDelegate.h
//  InitialsConverter
//
//  Created by Valiantsin Vasiliavitski on 5/2/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

